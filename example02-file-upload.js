// Allow us to use the Express frameworks
var express = require('express');

// Setup a new Express app
var app = express();

// The app should listen on port 3000, unless a different
// port is specified in the environment.
app.set('port', process.env.PORT || 3000);

// Specify that the app should use handlebars
var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

// Specify that the app should use the Formidable form processing library
// (must be installed with npm install --save formidable).
var formidable = require('formidable');

// Specify that when we browse to "/" with a GET request, show the example02/form view
app.get('/', function (req, res) {

    res.render("example02/form");
});

// Specify that when we browse to "/" with a POST request, do a file upload.
app.post('/', function (req, res) {

    // Parse the form using Formidable
    var form = new formidable.IncomingForm();

    // By default, Formidable will save uploaded files to some random temp directory somewhere.
    // This will change the File's upload location to somewhere we can control better - such as
    // an "uploads" folder within our project.
    // IMPORTANT: The folder you're uploading to MUST ALREADY EXIST or you will get an error!
    form.on("fileBegin", function (name, file) {
        // "name" will be the name of the <input> control on the form, while
        // "file" will contain info about the file itself.

        // Setting the file's path here will change where the file will be uploaded to.
        file.path = __dirname + "/uploads/" + file.name;
    });

    // This will begin parsing the form, and the given function will be called
    // once everything's complete.
    form.parse(req, function (err, fields, files) {
        // This function will be called once Formidable has finished parsing the form.

        // The "fields" argument contains all the "normal" fields (e.g. name, email etc)
        // and can be used just like req.body in example01.
        console.log(fields.simpleName);
        console.log(fields.simpleEmail);

        // The "files" argument contains information about any files which were uploaded, and
        // gives us the ability to access that file's data.
        // "fileUpload" here refers to the <input type="file" name="fileUpload"...> which we have
        // on our HTML form.
        var fileName = files.fileUpload.name; // The name of the file
        var fileSize = files.fileUpload.size; // The size, in bytes
        var fileType = files.fileUpload.type; // The type of file, e.g. "image/png"
        // console.log(files.fileUpload); // Uncomment this line and check the debug console to see many more potentially useful info you can get.

        var data = {
            name: fields.simpleName,
            email: fields.simpleEmail,
            fileName: fileName,
            fileSize: fileSize,
            fileType: fileType
        };
        res.render("example02/submitForm", data);
    });
});

// Allow the server to serve up files from the "uploads" folder.
app.use(express.static(__dirname + "/uploads"));

/* We haven't specified any other routes, so browsing anywhere other than "/" will result in a default error page being returned. */

// Start the server running.
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});