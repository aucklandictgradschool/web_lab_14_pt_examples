// Allow us to use the Express frameworks
var express = require('express');

// Setup a new Express app
var app = express();

// The app should listen on port 3000, unless a different
// port is specified in the environment.
app.set('port', process.env.PORT || 3000);

// Specify that the app should use handlebars
var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

// Specify that the app should use body parser (for reading submitted form data)
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true })); // Allows us to read forms submitted with POST requests

// Specify that when we browse to "/" with a GET request, show the example01/form view
app.get('/', function (req, res) {

    res.render("example01/form");
});

// Specify that when we browse to "/" with a POST request, read the submitted form values and then render submitForm.handlebars.
app.post('/', function (req, res) {

    // Read the form data
    var data = {
        name: req.body.name,
        options: req.body.options
    };

    res.render("example01/submitForm", data);
});

/* We haven't specified any other routes, so browsing anywhere other than "/" will result in a default error page being returned. */

// Start the server running.
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});